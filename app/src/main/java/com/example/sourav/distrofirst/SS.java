package com.example.sourav.distrofirst;

import android.app.Activity;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;

public class SS extends Thread {
    private MainActivity activity;
    public SS(MainActivity activity) {
        this.activity = activity;
    }
    private ServerSocket serverSocket;
    private Socket socket;
    private String message = "";
    @Override
    public void run() {
        try {
            int count = 0;
            while (true) {
                serverSocket = new ServerSocket(6000);
                socket = serverSocket.accept();
                count++;
                message = "Client Information\n" + socket.getInetAddress() + " : " + socket.getPort();
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        activity.msg.setText(message);
                    }
                });
                SSReplyThread ssReplyThread = new SSReplyThread(socket, count, activity);
                ssReplyThread.start();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
class SSReplyThread extends Thread {
    private String response = "";
    private Socket hostSocket;
    private int count;
    private MainActivity activity;
    public SSReplyThread(Socket hostSocket, int count, MainActivity activity) {
        this.hostSocket = hostSocket;
        this.count = count;
        this.activity = activity;
    }
    @Override
    public void run() {
        try {
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(activity);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            if (!sharedPreferences.contains("response")) {
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(
                        1024);
                byte[] buffer = new byte[1024];

                int bytesRead;
                InputStream inputStream = hostSocket.getInputStream();
                while ((bytesRead = inputStream.read(buffer)) != -1) {
                    byteArrayOutputStream.write(buffer, 0, bytesRead);
                    response += byteArrayOutputStream.toString("UTF-8");
                }
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        activity.clientMessage.setText(response);
                    }
                });
                editor.putString("response", response);
                editor.commit();
                if (!response.equalsIgnoreCase(""))
                    hostSocket.close();
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        activity.serverState.setText("Server closed\nRestart again");
                    }
                });
            }
            String s = sharedPreferences.getString("response", "");
            OutputStream outputStream = hostSocket.getOutputStream();
            PrintStream printStream = new PrintStream(outputStream);
            printStream.print(s);
            Log.e("Replied", "Replied" + Integer.toString(count));
            activity.sendData.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        hostSocket.close();
                        activity.serverState.setText("Data sent\nServer closed\nPlease restart if required");
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            });
        } catch (Exception e) {
            Log.e("ssReply",e.toString());
        }
    }
}
