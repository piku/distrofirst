package com.example.sourav.distrofirst;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


public class MainActivity extends AppCompatActivity {

    public TextView msg;
    public Button startServer;
    public Button sendData;
    public TextView serverState;
    public TextView clientMessage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        msg = (TextView) findViewById(R.id.msg);
        serverState = (TextView) findViewById(R.id.serverState);
        startServer = (Button) findViewById(R.id.start_server);
        sendData = (Button) findViewById(R.id.send_data);
        clientMessage = (TextView) findViewById(R.id.client_message);
        startServer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SS ss = new SS(MainActivity.this);
                ss.start();
                serverState.setText("Server Started\nready to send");
            }
        });
    }

}
